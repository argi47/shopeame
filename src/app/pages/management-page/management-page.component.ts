import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/shared/services/product.service';

@Component({
  selector: 'app-management-page',
  templateUrl: './management-page.component.html',
  styleUrls: ['./management-page.component.scss']
})
export class ManagementPageComponent implements OnInit {

  highestId = 0;              // Variable que almacena el ID de los nuevos productos
  addProductForm;
  imageUrl;                   // Variable que contiene la imagen subida, convertida en texto para pasarla a la db.json
  item;                       // Variable que pasa los datos del form al componente card para ver el preview
  submitted = false;          // Indica si hemos intentado enviar el formulario
  formIsValid = false;        // Indica si el formulario es válido
  productToEdit: any = {};    // Contiene el producto que querremos editar, si se da el caso

  constructor(private formBuilder: FormBuilder, private productService: ProductService, public router: Router) { }

  ngOnInit(): void {
    this.createHighestId();
    this.productToEdit = this.productService.getProductToEdit();
    this.item = this.productToEdit;    

    if (this.productToEdit !== "") {

      this.addProductForm = this.formBuilder.group({
        id: [this.productToEdit.id, Validators.required],
        name: [this.productToEdit.name, [Validators.required, Validators.minLength(5), Validators.maxLength(20)]],
        price: [this.productToEdit.price, [Validators.required, Validators.min(1), Validators.max(999999)]],
        description: [this.productToEdit.description, [Validators.required, Validators.minLength(10), Validators.maxLength(50)]],
        stars: [this.productToEdit.stars, [Validators.required, Validators.min(0), Validators.max(5), Validators.minLength(1), Validators.maxLength(4)]],
        image: ['', this.productToEdit !== "" ? '' : Validators.required]
      });

    } else {

      this.addProductForm = this.formBuilder.group({    
        id: ['0', Validators.required],      
        name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]],
        price: ['', [Validators.required, Validators.min(1), Validators.max(999999)]],
        description: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(50)]],
        stars: ['', [Validators.required, Validators.min(0), Validators.max(5), Validators.minLength(1), Validators.maxLength(4)]],
        image: ['', Validators.required]
      });

    }
    
    this.showValueChanges();    
    
    console.log(this.productToEdit);
    
  }

  // formIsValid() {
  //   this.addProductForm.statusChanges.subscribe((changes) => {
  //     this.isValid = changes;
  //   });
  // }

  showValueChanges() {
    this.addProductForm.valueChanges.subscribe((changes) => {
      changes.image = this.imageUrl;
      // console.log(this.addProductForm.value);
      if (this.productToEdit !== "" && this.addProductForm.value.image === undefined) {
        this.item.name = changes.name;
        this.item.price = changes.price;
        this.item.description = changes.description;
        this.item.stars = changes.stars;
      } else {
        this.item = changes;
      }
      
    });
  }

  createHighestId() {    
    this.productService.getProducts().subscribe((res: any) => {
      for(let i=0;i<res.length;i++) {
        this.highestId = res[i].id > this.highestId ? res[i].id : this.highestId;
      }     
      this.highestId += 1;
    })
  }

  file(file) {
    return new Promise((res, rej) => {
      const reader = new FileReader();
      reader.onload = e => res(e.target.result);
      reader.onerror = e => rej(e);
      reader.readAsDataURL(file);
    }​​);
  }​​;

  async preview(event) {
    const file = event.target.files[0];
    const url = await this.file(file);
    this.imageUrl = url;
    this.showValueChanges();
  }

  addProduct() {
    this.submitted = true;
    console.log(this.highestId);
    const product = {...this.addProductForm.value, 
      image: this.productToEdit !== "" && this.imageUrl === undefined ? this.productToEdit.image : this.imageUrl,
      id: this.productToEdit !== "" ? this.productToEdit.id : this.highestId 
    }
    // const product = {...this.addProductForm.value, image: this.imageUrl}    
    console.log(this.addProductForm.status);
    if (this.addProductForm.status === 'VALID') {
      if (this.productToEdit !== "") {        
        this.productService.updateProduct(product).subscribe();
        this.productService.unsetProduct();
      } else {
        this.productService.addProduct(product).subscribe();
      }      
      this.formIsValid = true;

      setTimeout(() => { 
        this.router.navigateByUrl('/products');
       }, 5000);
    }
    // console.log(product);
    // console.log(this.addProductForm.value);
  }

}
