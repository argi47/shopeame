import { Component, OnInit } from '@angular/core';
import { faList, faThLarge } from '@fortawesome/free-solid-svg-icons';
import { ProductService } from 'src/app/shared/services/product.service';

@Component({
  selector: 'app-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.scss']
})
export class ProductsPageComponent implements OnInit {

  products; // Products shown on the page
  allProducts; // All existent products
  numOfProducts; // Number of products shown on the page

  grid = true; // If true, show the products on grid mode. Else, show it like list

  // FontAwesome icons
  faThLarge = faThLarge;
  faList = faList;

  constructor(private productsService: ProductService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.productsService.getProducts().subscribe((res) => {
      this.products = res;
      this.allProducts = res;
      this.numOfProducts = this.products.length;
      console.log(this.products);
      // console.log(this.numOfProducts);
    })
  }

  searchProduct(event) {
    this.products = this.allProducts.filter(product => product.name.toLowerCase().includes(event.target.value.toLowerCase()));
    this.numOfProducts = this.products.length;
  }

  showList() {
    this.grid = false;
    const listButton$$ = document.querySelector(`[data-function=btn-list]`);
    const gridButton$$ = document.querySelector(`[data-function=btn-grid]`);
    listButton$$.classList.add('c-products__btn-list--selected');
    gridButton$$.classList.remove('c-products__btn-grid--selected');
  }

  showGrid() {
    this.grid = true;
    const listButton$$ = document.querySelector(`[data-function=btn-list]`);
    const gridButton$$ = document.querySelector(`[data-function=btn-grid]`);
    listButton$$.classList.remove('c-products__btn-list--selected');
    gridButton$$.classList.add('c-products__btn-grid--selected');
  }

  deleteFromLocal(id) {
    this.products = this.products.filter(item => item.id !== id);
    this.allProducts = this.allProducts.filter(prod => prod.id !== id);    
    this.numOfProducts = this.products.length;
  }

}
