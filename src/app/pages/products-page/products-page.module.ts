import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsPageRoutingModule } from './products-page-routing.module';
import { ProductsPageComponent } from './products-page.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { GalleryComponent } from './components/gallery/gallery.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


@NgModule({
  declarations: [ProductsPageComponent, GalleryComponent],
  imports: [
    CommonModule,
    ProductsPageRoutingModule,
    SharedModule,
    FontAwesomeModule
  ]
})
export class ProductsPageModule { }
