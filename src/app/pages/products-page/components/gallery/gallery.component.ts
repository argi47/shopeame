import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  @Input() list;
  @Input() gridView;
  @Output() idEmitter = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  // deleteFromLocal(id) {
  //   this.list = this.list.filter(item => item.id !== id);    
  // }

}
