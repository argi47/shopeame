import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../../services/product.service';
// import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  styles: [`
    .star {
      position: relative;
      display: inline-block;
      font-size: 1.5rem;
      color: #d3d3d3;
    }
    .full {
      color: red;
    }
    .half {
      position: absolute;
      display: inline-block;
      overflow: hidden;
      color: #FB8200;
    }
  `]
  // providers: [NgbRatingConfig] // add NgbRatingConfig to the component providers
})
export class CardComponent implements OnInit {

  @Input() data;
  @Input() edit = true;
  @Output() productIdEmitter = new EventEmitter();

  constructor (public router: Router, private productService: ProductService) {}

  // constructor(config: NgbRatingConfig) { 
  //   // customize default values of ratings used by this component tree
  //   config.max = 5;
  //   config.readonly = true;
  // }

  ngOnInit(): void {
  }

  navToManagement(product){
    this.productService.setProduct(product);
    this.router.navigateByUrl('/management');
  }

  deleteProduct(id) {
    this.productService.removeProduct(id).subscribe();
    this.productIdEmitter.emit(id);
  }

}
