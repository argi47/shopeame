import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  product: any = "";

  constructor(private http: HttpClient) { }

  getProducts(){
    return this.http.get('http://localhost:3000/products')
  }

  addProduct(newProduct) {
    return this.http.post('http://localhost:3000/products', newProduct)
  }

  updateProduct(updatedProduct) {
    return this.http.put('http://localhost:3000/products/' + updatedProduct.id, updatedProduct)
  }

  removeProduct(productId) {
    return this.http.delete('http://localhost:3000/products/' + productId)
  }

  getProductToEdit() {
    return this.product;
  }

  setProduct(product) {
    this.product = product;
  }

  unsetProduct() {
    this.product = "";
  }

}
