import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'asterisks'
})
export class AsterisksPipe implements PipeTransform {

  transform(value, arg) {
    if (arg <= 10) {
      return value + ' (BARATO!)';
    } else {
      return value;
    }
  }

}
