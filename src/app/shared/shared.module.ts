import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card/card.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AsterisksPipe } from './pipes/asterisks.pipe';

@NgModule({
  declarations: [CardComponent, AsterisksPipe],
  imports: [
    CommonModule,
    NgbModule
  ],
  exports: [CardComponent, AsterisksPipe]
})
export class SharedModule { }
