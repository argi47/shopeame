import { Component, OnInit } from '@angular/core';
import { faBinoculars, faDharmachakra, faRadiation } from '@fortawesome/free-solid-svg-icons';
import { ProductService } from 'src/app/shared/services/product.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  faRadiation = faRadiation;
  faBinoculars = faBinoculars;
  faDharmachakra = faDharmachakra; 

  constructor(private productsService: ProductService) { }

  ngOnInit(): void {
  }

  unsetEditableProduct() {
    this.productsService.unsetProduct();
  }

}